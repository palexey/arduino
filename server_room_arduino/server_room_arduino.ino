#include <Adafruit_AHTX0.h>
#include <Wire.h>


Adafruit_AHTX0 aht;

const uint8_t water_PIN = 2;
const uint8_t zumer_PIN = A7;
const uint8_t gerkon_PIN = 5;
const uint8_t LED_PIN = LED_BUILTIN;

const int COMBINATION[] = { HIGH, LOW, HIGH };  // Пример комбинации сигналов
const int COMBINATION_LENGTH = sizeof(COMBINATION) / sizeof(COMBINATION[0]);
unsigned long lastSignalTime = 0;
bool waitingForSignal = false;
int receivedSignals[COMBINATION_LENGTH];
int receivedSignalsCount = 0;

void setup() {
  Serial.begin(115200);
  delay(100);

  Wire.begin();

  if (aht.begin()) {
    Serial.println("Found AHT10");
  } else {
    Serial.println("Not found AHT10");
  }

  pinMode(water_PIN, INPUT);
  pinMode(zumer_PIN, OUTPUT);

  pinMode(gerkon_PIN, INPUT);

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
}

void loop() {
  //aht_sensor();
  //water_PINensor();

  byte water_status = digitalRead(water_PIN);
  byte gerkon_status = digitalRead(gerkon_PIN);
  Serial.print("GERKON sensor status:  ");
  Serial.println(gerkon_status);
  Serial.print("Water sensor status:  ");
  Serial.println(water_status);
  delay(500);
}

void water(byte water_status) {
  if (water_status == 0) {
    Serial.print("Water sensor status:  ");
    Serial.println(water_status);
    digitalWrite(LED_PIN, LOW);
  } else {
    digitalWrite(LED_PIN, HIGH);
  }
}

void gerkon(byte gerkon_status) {
  // Проверяем, потерян ли сигнал с D1
  if (digitalRead(gerkon_PIN) == LOW && !waitingForSignal) {
    Serial.println("Сигнал с D1 потерян. Введите комбинацию сигналов:");
    // Ожидаем ввода правильной комбинации
    while (receivedSignalsCount < COMBINATION_LENGTH) {
      // Считываем сигналы с D1 и сохраняем в массив
      int signal = digitalRead(gerkon_PIN);
      receivedSignals[receivedSignalsCount] = signal;
      receivedSignalsCount++;
      // Ожидаем некоторое время между сигналами
      delay(500);
    }
    // Проверяем, совпадает ли введенная комбинация с правильной
    bool isCorrect = true;
    for (int i = 0; i < COMBINATION_LENGTH; i++) {
      if (receivedSignals[i] != COMBINATION[i]) {
        isCorrect = false;
        break;
      }
    }
    if (isCorrect) {
      Serial.println("Правильная комбинация введена.");
    } else {
      Serial.println("Неправильная комбинация. Подача сигнала через 5 минут.");
      waitingForSignal = true;
      lastSignalTime = millis();
    }
  }
  // Если ожидаем подачу сигнала через 5 минут, проверяем время
  if (waitingForSignal && millis() - lastSignalTime >= 5 * 60 * 1000) {
    // Подаем сигнал на D7
    digitalWrite(zumer_PIN, HIGH);
    digitalWrite(LED_PIN, HIGH);
    Serial.println("Сигнал подан на zumer.");
    waitingForSignal = false;
  }
}
