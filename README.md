# My Arduino Project

_RTC DS1302:_
```    
    VCC GND CLK DAT RST
```    
_Temp HW-481 v0.2_
```
    Signal +5V GND
```
_Mod-SD (card reader)_
```
    GND +3V SV CS MOSI SCK MISO GND
```
_WiFi ESP01_

![Alt text](image/image.png)
![Alt text](image/image-1.png)


_ESP8266_

![ESP8266 board pin out](image/board_pinouts.png)

_ESP8266 Wemos mini_

![ESP8266 wemos mini](image/wemos.png)

_Повышающий модуль распиновка_
![Многофункциональная повышающая мини-плата 5 В/8 В/9 В/12 В](image/upvolt.png)
