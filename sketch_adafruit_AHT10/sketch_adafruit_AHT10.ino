#include <Adafruit_AHTX0.h>                               // Подключаем библиотеку Adafruit_AHTX0
Adafruit_AHTX0 aht;                                       // Создаём объект для работы с библиотекой

void setup() 
{
  Wire.begin(D5, D6);
  Serial.begin(115200);                                   // Инициируем работу с монитором последовательного порта на скорости 115200 бод
  Serial.println("Adafruit AHT10/AHT20 demo!");           // Выводим собощение

  if (!aht.begin())                                       // Инициализация датчика
  {                                    
    Serial.println("Could not find AHT? Check wiring");   // Отправка сообщения
    while (1) delay(10);                                  // Зацикливаем программу             
  } 
  Serial.println("AHT10 or AHT20 found");                 // Отправка сообщения
}

void loop() 
{
  sensors_event_t humidity, temp;                         // Создаём объект для работы с библиотекой
  aht.getEvent(&humidity, &temp);                         // Считваем показания
  Serial.print("Temperature: ");                          // Отправка сообщения
  Serial.print(temp.temperature);                         // Отпрака температуры
  Serial.println(" degrees C");                           // Отправка сообщения 
  Serial.print("Humidity: ");                             // Отправка сообщения
  Serial.print(humidity.relative_humidity);               // Отправка влажности 
  Serial.println("% rH");                                 // Отправка сообщения
  delay(500);                                             // Пауза
} 