
boolean LEDflag = false;
uint32_t myTimer; // переменная времени

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  if (millis() - myTimer >= 1000) {
    myTimer = millis(); // сбросить таймер
    digitalWrite(LED_BUILTIN, LEDflag); // вкл/выкл
    LEDflag = !LEDflag; // инвертировать флаг
  }
}
