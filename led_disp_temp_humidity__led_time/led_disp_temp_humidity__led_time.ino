#include <DHT.h>
#include <DHT_U.h>

//#include <Ds1302.h>

#include <Wire.h>               // библиотека для управления устройствами по I2C
#include <LiquidCrystal_I2C.h>  // подключаем библиотеку для QAPASS 1602
#include <LCD_1602_RUS.h>
#include <AHT10.h>  // Библиотека датчика
#include <RTClib.h>

//#include "iarduino_RTC.h"

AHT10Class AHT10;

LCD_1602_RUS LCD(0x27, 16, 2);  // присваиваем имя LCD для дисплея
//iarduino_RTC watch(RTC_DS1302,8,10,9);  // для модуля DS1302 - RST, CLK, DAT

DS1302 rtc(8, 10, 9);
char buf[20];

#define RED 3
#define GRN 5
#define BLU 6

void setup() {
  delay(300);

  Wire.begin();

  Serial.begin(115200);  // Инициализируем монитор порта
                         //Serial.println();                                // Печатаем пустую строку
                         //delay(2000);                                     // Пауза в 2 секунды

  LCD.init();       // инициализация LCD дисплея
  LCD.backlight();  // включение подсветки дисплея
  LCD.noDisplay();  // выключаем и включаем надпись на дисплее

  AHT10.begin(0x38);  // Инициализируем датчик с адресом 0x38

  // RGB-led set
  pinMode(RED, OUTPUT);
  pinMode(GRN, OUTPUT);
  pinMode(BLU, OUTPUT);

  // watch set
  //watch.begin();
  //watch.settime(0, 16, 15, 14, 9, 23, 5); // 0  сек, 30 мин, 18 часов, 12, июня, 2020, четверг
  rtc.begin();
  if (!rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(__DATE__, __TIME__));
  }
}

void loop() {

  String str_t;
  str_t = "Темп: ";
  String str_v;
  str_v = "Влаж: ";

  float TemperC = AHT10.GetTemperature();  // Считываем показание температуры
  float HumidiP = AHT10.GetHumidity();     // Считываем показание влажности
  // float D = AHT10.GetDewPoint();                   // Считываем значение точки росы для данной влажности

  LCD.setCursor(0, 0);   // ставим курсор на 1 символ первой строки
  LCD.print(str_t);      // Печатаем "Температура: "
  LCD.print(TemperC);    // Печатаем показание температуры
  LCD.println(" *C  ");  // Печатаем " *C"

  LCD.setCursor(0, 1);   // ставим курсор на 1 символ 2 строки
  LCD.print(str_v);      // Печатаем "Влажность: "
  LCD.print(HumidiP);    // Печатаем показание влажности
  LCD.println(" %   ");  // Печатаем " %"

  LCD.noDisplay();  // выключаем и включаем надпись на дисплее
  delay(500);
  LCD.display();
  delay(500);

  /*  
  if (millis() % 1000 == 0) {
    LCD.setCursor(0,0);
    LCD.println(watch.gettime("d M Y, D"));
    LCD.setCursor(4,1);
    LCD.println(watch.gettime("H:i:s"));
    delay(1);
  }
  
  if(millis()%1000==0){                                   // Если прошла 1 секунда.
    //Serial.println(watch.gettime("d-m-Y, H:i:s, D"));     // Выводим время.
    Serial.println(watch.gettime("s"));
    delay(1);                                             // Приостанавливаем скетч на 1 мс, чтоб не выводить время несколько раз за 1мс.
  }
  */

  DateTime now = rtc.now();

  Serial.println(now.tostr(buf));
  /*
  Serial.print(" since midnight 1970/01/01 = ");
  Serial.print(now.unixtime());
  Serial.print("s = ");
  Serial.print(now.unixtime() / 86400L);
  Serial.println("d");

  // calculate a date which is 7 days and 30 seconds into the future
  DateTime future(now + (7 * 86400L + 30));

  Serial.print(" now + 7d + 30s: ");
  Serial.println(future.tostr(buf));

  // calculate a date which is 30 days before
  DateTime past(now - TimeDelta(30 * 86400L));

  Serial.print(" now - 30d: ");
  Serial.println(past.tostr(buf));
  delay(1000);
  */

  Serial.println();

  Serial.print("Температура: ");  // Печатаем "Температура: "
  Serial.print(TemperC);          // Печатаем показание температуры
  Serial.println(" *C");          // Печатаем " *C"

  Serial.print("Влажность: ");  // Печатаем "Влажность: "
  Serial.print(HumidiP);        // Печатаем показание влажности
  Serial.println(" %");         // Печатаем " %"

  for (int i = 0; i <= 255; i++) {
    analogWrite(RED, i);
    delay(2);
  }
  for (int i = 255; i >= 0; i--) {
    analogWrite(RED, i);
    delay(2);
  }

  // плавное включение/выключение зеленого цвета
  for (int i = 0; i <= 255; i++) {
    analogWrite(GRN, i);
    delay(2);
  }
  for (int i = 255; i >= 0; i--) {
    analogWrite(GRN, i);
    delay(2);
  }


  // плавное включение/выключение синего цвета
  for (int i = 0; i <= 255; i++) {
    analogWrite(BLU, i);
    delay(2);
  }
  for (int i = 255; i >= 0; i--) {
    analogWrite(BLU, i);
    delay(2);
  }

  //analogWrite(RED, HIGH); // включаем красный свет
  //analogWrite(GRN, LOW);
  //analogWrite(BLU, LOW);

  // delay(1000); // устанавливаем паузу для эффекта

  //analogWrite(RED, LOW);
  //analogWrite(GRN, HIGH); // включаем зеленый свет
  //analogWrite(BLU, LOW);

  //delay(1000); // устанавливаем паузу для эффекта

  //analogWrite(RED, LOW);
  //analogWrite(GRN, LOW);
  //analogWrite(BLU, HIGH); // включаем синий свет

  //delay(1000); // устанавливаем паузу для эффекта
}