
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(50);                     // wait for a second
  digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
  delay(50);                     // wait for a second

  digitalWrite(4, 0);
  delay(500);
  digitalWrite(4, 1);
  delay(500);
  digitalWrite(5, 0);
  delay(500);
  digitalWrite(5, 1);
  delay(500);
  digitalWrite(6, 0);
  delay(500);
  digitalWrite(6, 1);
  delay(500);
  digitalWrite(7, 0);
  delay(500);
  digitalWrite(7, 1);
  delay(500);

}
