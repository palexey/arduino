#include "IRremote.h"  // подключаем библиотеку ИК приемника
IRrecv irrecv(15);     // указываем пин для ИК приемника
decode_results results;


void setup() {
  Serial.begin(115200);  // подключаем монитор порта
  irrecv.enableIRIn();   // подключаем прием ИК сигнала

  //pinMode(A0, INPUT);  // указываем пин для ИК приемника
}

void loop() {

  if (irrecv.decode(&results)) {      // если данные пришли
    Serial.println(results.value, HEX);  // выводим данные
    irrecv.resume();                  // принимаем следующую команду
  }
}