
void setup() {
  Serial.begin(115200);
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {

  digitalWrite(LED_BUILTIN, 50);   // turn the LED on (HIGH is the voltage level)
  delay(100);                      // wait for a second
  digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
  delay(100);                      // wait for a second
  digitalWrite(LED_BUILTIN, 50);   // turn the LED on (HIGH is the voltage level)
  delay(200);                      // wait for a second
  digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
  delay(300);                      // wait for a second
  digitalWrite(LED_BUILTIN, 50);   // turn the LED on (HIGH is the voltage level)
  delay(400);                      // wait for a second
  digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
  delay(500);                      // wait for a second
  digitalWrite(LED_BUILTIN, 50);   // turn the LED on (HIGH is the voltage level)
  delay(600);                      // wait for a second
  digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
  delay(700);                      // wait for a second
  digitalWrite(LED_BUILTIN, 50);   // turn the LED on (HIGH is the voltage level)
  delay(800);                      // wait for a second
  digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
  delay(900);                      // wait for a second
}
