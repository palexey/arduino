#include <Wire.h> // библиотека для управления устройствами по I2C 
#include <LiquidCrystal_I2C.h> // подключаем библиотеку для QAPASS 1602
#include <LCD_1602_RUS.h>
#include <AHT10.h>                                 // Библиотека датчика
#include "iarduino_RTC.h"


AHT10Class AHT10; 

//LiquidCrystal_I2C LCD(0x27,16,2); // присваиваем имя LCD для дисплея
LCD_1602_RUS LCD(0x27,0,0); // присваиваем имя LCD для дисплея
iarduino_RTC time(RTC_DS1302,8,9,10);  // для модуля DS1302 - RST, CLK, DAT

#define RED 3
#define GRN 4
#define BLU 5

void setup() {

   Wire.begin(); 

   Serial.begin(9600);                             // Инициализируем монитор порта на скорости 9600
   Serial.println();                                // Печатаем пустую строку
   delay(2000);                                     // Пауза в 2 секунды

   LCD.init(); // инициализация LCD дисплея
   LCD.backlight(); // включение подсветки дисплея
  
  if(AHT10.begin(0x38))                            // Инициализируем датчик с адресом 0x38
    Serial.println("AHT10 подключен.");            // Если удачно печатаем "AHT10 подключен."
  else{
    Serial.println("AHT10 не подключен.");         // Если не удачно печатаем "AHT10 не подключен."
    while(1);                                      // Заканчиваем выполнение
  } 

  LCD.noDisplay(); // выключаем и включаем надпись на дисплее
  delay(1000);

  pinMode(RED, OUTPUT);  // используем Pin11 для вывода
  pinMode(GRN, OUTPUT); // используем Pin12 для вывода
  pinMode(BLU, OUTPUT);  // используем Pin13 для вывода

}

void loop() {

  String str_t;
  str_t = "Темп: ";
  String str_v;
  str_v = "Влаж: ";

  float T = AHT10.GetTemperature();                // Считываем показание температуры
  float H = AHT10.GetHumidity();                   // Считываем показание влажности
  // float D = AHT10.GetDewPoint();                   // Считываем значение точки росы для данной влажности

  LCD.setCursor(0, 0);     // ставим курсор на 1 символ первой строки
  LCD.print(str_t);                   // Печатаем "Температура: "
  LCD.print(T);                                 // Печатаем показание температуры 
  LCD.println(" *C  ");                           // Печатаем " *C"

  LCD.setCursor(0, 1);     // ставим курсор на 1 символ 2 строки
  LCD.print(str_v);                     // Печатаем "Влажность: "
  LCD.print(H);                                 // Печатаем показание влажности
  LCD.println(" %   ");                            // Печатаем " %"
   
  LCD.noDisplay(); // выключаем и включаем надпись на дисплее
  delay(1000);
  LCD.display();
  delay(1000);
/*
  Serial.print("Температура: ");                   // Печатаем "Температура: "
  Serial.print(T);                                 // Печатаем показание температуры 
  Serial.println(" *C");                           // Печатаем " *C"

  Serial.print("Влажность: ");                     // Печатаем "Влажность: "
  Serial.print(H);                                 // Печатаем показание влажности
  Serial.println(" %");                            // Печатаем " %"
*/

/*
   for (int i = 0; i <= 255; i++) {
      analogWrite(RED, i);
      delay(2);
   }
   for (int i = 255; i >= 0; i--) {
      analogWrite(RED, i);
      delay(2);
   }

   // плавное включение/выключение зеленого цвета
   for (int i = 0; i <= 255; i++) {
      analogWrite(GRN, i);
      delay(2);
   }
   for (int i = 255; i >= 0; i--) {
      analogWrite(GRN, i);
      delay(2);
   }
*/
analogWrite(RED, 0);
analogWrite(GRN, 0);

   // плавное включение/выключение синего цвета
   for (int i = 0; i <= 255; i++) {
      analogWrite(BLU, i);
      delay(2);
   }
   for (int i = 255; i >= 0; i--) {
      analogWrite(BLU, i);
      delay(2);
   }

  //analogWrite(RED, HIGH); // включаем красный свет
  //analogWrite(GRN, LOW);
  //analogWrite(BLU, LOW);
 
  // delay(1000); // устанавливаем паузу для эффекта
 
  //analogWrite(RED, LOW);
  //analogWrite(GRN, HIGH); // включаем зеленый свет
  //analogWrite(BLU, LOW);
 
  //delay(1000); // устанавливаем паузу для эффекта
 
  //analogWrite(RED, LOW);
  //analogWrite(GRN, LOW);
  //analogWrite(BLU, HIGH); // включаем синий свет
 
  //delay(1000); // устанавливаем паузу для эффекта
}