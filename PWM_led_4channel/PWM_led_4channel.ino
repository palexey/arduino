
#include "GyverButton.h"

#define EB_NO_FOR
#define EB_NO_COUNTER

#define LedPin10 10  // задаем имя для Pin3
#define LedPin5 5    // задаем имя для Pin5
#define LedPin6 6    // задаем имя для Pin6
#define LedPin9 9    // задаем имя для Pin6
#define BTN_PIN1 2
#define BTN_PIN2 12

GButton butt1(BTN_PIN1);
GButton butt2(BTN_PIN2);

int previousMillis = 0;  // Stores the previous time

void setup() {

  Serial.begin(115200);
  // Пины D5 и D6 - 62.5 кГц
  // TCCR0B = 0b00000001;  // x1
  // TCCR0A = 0b00000011;  // fast pwm
  // Пины D5 и D6 - 31.4 кГц
  //TCCR0B = 0b00000001;  // x1
  //TCCR0A = 0b00000001;  // phase correct
  // Пины D5 и D6 - 7.8 кГц
  //TCCR0B = 0b00000010;  // x8
  //TCCR0A = 0b00000011;  // fast pwm
  // Пины D5 и D6 - 4 кГц
  //TCCR0B = 0b00000010;        // x8
  //TCCR0A = 0b00000001;        // phase correct
  pinMode(LedPin10, OUTPUT);  // инициализируем Pin6 как выход
  pinMode(LedPin5, OUTPUT);   // инициализируем Pin6 как выход
  pinMode(LedPin6, OUTPUT);   // инициализируем Pin6 как выход
  pinMode(LedPin9, OUTPUT);   // инициализируем Pin6 как выход

/*
  butt1.setTimeout(300);       // настройка таймаута на удержание (по умолчанию 500 мс)
  butt1.setClickTimeout(600);  // настройка таймаута между кликами (по умолчанию 300 мс)
  butt1.setType(HIGH_PULL);
  butt1.setDirection(NORM_OPEN);
  butt2.setTimeout(300);       // настройка таймаута на удержание (по умолчанию 500 мс)
  butt2.setClickTimeout(600);  // настройка таймаута между кликами (по умолчанию 300 мс)
  butt2.setType(HIGH_PULL);
  butt2.setDirection(NORM_OPEN);
*/  
}

void loop() {

/*
  butt1.tick();
  butt2.tick();
  if (butt1.state()) Serial.println("Hold");   // возвращает состояние кнопки
  if (butt2.state()) Serial.println("Hold2");  // возвращает состояние кнопки
*/  

  LedPin_5();
  analogWrite(LedPin5, LOW);
  analogWrite(LedPin6, LOW);
  analogWrite(LedPin9, LOW);
  analogWrite(LedPin10, LOW);
  /*
  LedPin_6();
  analogWrite(LedPin5, LOW);
  analogWrite(LedPin6, LOW);
  analogWrite(LedPin9, LOW);
  analogWrite(LedPin10, LOW);
    LedPin_9();
  analogWrite(LedPin5, LOW);
  analogWrite(LedPin6, LOW);
  analogWrite(LedPin9, LOW);
  analogWrite(LedPin10, LOW);
  LedPin_10();
  analogWrite(LedPin5, LOW);
  analogWrite(LedPin6, LOW);
  analogWrite(LedPin9, LOW);
  analogWrite(LedPin10, LOW);
  */
}

void LedPin_5() {
  Serial.print("LedPin5    ");
  Serial.println("Start");

  const int fadeDelay = 30;          // Delay between brightness changes in milliseconds
  const int fadeAmount = 5;          // Amount of brightness change in each step
  int brightness = 0;                // Current brightness of the LED
  int fadeDirection = 1;             // Direction of brightness change (1 for increasing, -1 for decreasing)
  unsigned long previousMillis = 0;  // Variable to store the previous time

  unsigned long currentMillis = millis();  // Get the current time
  if (currentMillis - previousMillis >= fadeDelay) {
    previousMillis = currentMillis;            // Save the current time
    brightness += fadeAmount * fadeDirection;  // Update brightness
    // Reverse the fade direction if brightness reaches its limits
    if (brightness <= 0 || brightness >= 240) {
      fadeDirection = -fadeDirection;
    }
  }
  analogWrite(LedPin5, brightness);
  analogWrite(LedPin6, brightness);
  analogWrite(LedPin9, brightness);
  analogWrite(LedPin10, brightness);
}

void LedPin_6() {
  Serial.print("LedPin 6    ");
  Serial.println("Start");

  const int fadeDelay = 50;          // Delay between brightness changes in milliseconds
  const int fadeAmount = 5;          // Amount of brightness change in each step
  int brightness = 0;                // Current brightness of the LED
  int fadeDirection = 1;             // Direction of brightness change (1 for increasing, -1 for decreasing)
  unsigned long previousMillis = 0;  // Variable to store the previous time

  unsigned long currentMillis = millis();  // Get the current time
  if (currentMillis - previousMillis >= fadeDelay) {
    previousMillis = currentMillis;            // Save the current time
    brightness += fadeAmount * fadeDirection;  // Update brightness
    // Reverse the fade direction if brightness reaches its limits
    if (brightness <= 0 || brightness >= 240) {
      fadeDirection = -fadeDirection;
    }
  }
  analogWrite(LedPin5, brightness);
  analogWrite(LedPin6, brightness);
  analogWrite(LedPin9, brightness);
  analogWrite(LedPin10, brightness);
}

void LedPin_9() {
  Serial.print("LedPin 9    ");
  Serial.println("Start");

  const int fadeDelay = 200;         // Delay between brightness changes in milliseconds
  const int fadeAmount = 5;          // Amount of brightness change in each step
  int brightness = 0;                // Current brightness of the LED
  int fadeDirection = 1;             // Direction of brightness change (1 for increasing, -1 for decreasing)
  unsigned long previousMillis = 0;  // Variable to store the previous time

  unsigned long currentMillis = millis();  // Get the current time
  if (currentMillis - previousMillis >= fadeDelay) {
    previousMillis = currentMillis;            // Save the current time
    brightness += fadeAmount * fadeDirection;  // Update brightness
    // Reverse the fade direction if brightness reaches its limits
    if (brightness <= 0 || brightness >= 240) {
      fadeDirection = -fadeDirection;
    }
  }
  analogWrite(LedPin5, brightness);
  analogWrite(LedPin6, brightness);
  analogWrite(LedPin9, brightness);
  analogWrite(LedPin10, brightness);
}

void LedPin_10() {
  Serial.print("LedPin 10    ");
  Serial.println("Start");
  const int fadeDelay = 400;         // Delay between brightness changes in milliseconds
  const int fadeAmount = 5;          // Amount of brightness change in each step
  int brightness = 0;                // Current brightness of the LED
  int fadeDirection = 1;             // Direction of brightness change (1 for increasing, -1 for decreasing)
  unsigned long previousMillis = 0;  // Variable to store the previous time

  unsigned long currentMillis = millis();  // Get the current time
  if (currentMillis - previousMillis >= fadeDelay) {
    previousMillis = currentMillis;            // Save the current time
    brightness += fadeAmount * fadeDirection;  // Update brightness
    // Reverse the fade direction if brightness reaches its limits
    if (brightness <= 0 || brightness >= 240) {
      fadeDirection = -fadeDirection;
    }
  }
  analogWrite(LedPin5, brightness);
  analogWrite(LedPin6, brightness);
  analogWrite(LedPin9, brightness);
  analogWrite(LedPin10, brightness);
}

void LedPin_11() {
  Serial.print("LedPin 11    ");
  Serial.println("Start");
  int fadeSpeed = 10;            // Speed of fading, higher value for slower fading
  int brightness = 0;            // Current brightness level
  int fadeAmount = 10;           // Increment/Decrement step for fading
  int previousMillis = 0;        // Stores the previous time
  int currentMillis = millis();  // Get the current time
  if (currentMillis - previousMillis >= fadeSpeed) {
    // Save the current time as previousMillis
    previousMillis = currentMillis;
    // Update the brightness level
    brightness = brightness + fadeAmount;
    // Reverse the fading direction if brightness reaches the limits
    if (brightness <= 0 || brightness >= 255) {
      fadeAmount = -fadeAmount;
    }
    // Apply the new brightness level to the LED
    analogWrite(LedPin5, brightness);
    analogWrite(LedPin6, brightness);
    analogWrite(LedPin9, brightness);
    analogWrite(LedPin10, brightness);
  }
  Serial.print("LedPin 11    ");
  Serial.println("Done");
}