#include <Wire.h>

#include <LiquidCrystal_I2C.h>  // подключаем библиотеку для QAPASS 1602
#include <LCD_1602_RUS.h>
LCD_1602_RUS LCD(0x27, 16, 2);  // присваиваем имя LCD для дисплея

#include <AHT10.h>  // Библиотека датчика
AHT10Class AHT10;

#define str_t "Температура: "
#define str_v "Влажность:   "

void setup() {
  delay(300);

  Wire.begin();

  Serial.begin(115200);  // Инициализируем монитор порта

  LCD.init();       // инициализация LCD дисплея
  LCD.backlight();  // включение подсветки дисплея

  AHT10.begin(0x38);  // Инициализируем датчик с адресом 0x38
}

void loop() {

  lcd_temp_c();
  delay(1000);
  LCD.clear();

  lcd_humi();
  delay(1000);
  LCD.clear();
}

void lcd_temp_c() {

  float TemperC = AHT10.GetTemperature();  // Считываем показание температуры

  LCD.setCursor(0, 0);   // ставим курсор на 1 символ первой строки
  LCD.print(str_t);      // Печатаем "Температура: "
  LCD.print(TemperC);    // Печатаем показание температуры
  LCD.println(" *C  ");  // Печатаем " *C"
  LCD.setCursor(0, 1);   // ставим курсор на 1 символ 2 строки
  LCD.print("Temp2: ");  // Печатаем "Температура: "
  LCD.println(" *C  ");  // Печатаем " *C"
}
void lcd_humi() {

  float HumidiP = AHT10.GetHumidity();  // Считываем показание влажности

  LCD.setCursor(0, 0);   // ставим курсор на 1 символ первой строки
  LCD.print(str_v);      // Печатаем "Влажность: "
  LCD.print(HumidiP);    // Печатаем показание влажности
  LCD.println(" %   ");  // Печатаем " %"
  LCD.setCursor(0, 1);   // ставим курсор на 1 символ 2 строки
  LCD.print("Humi2: ");  // Печатаем "Влажность: "
  LCD.println(" %   ");  // Печатаем " %"
}