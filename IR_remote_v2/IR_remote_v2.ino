#define IN_PIN 3       // входной пин
#define LOG_SIZE 200    // размер лога (штук)
// массив измерений. Для импульсов короче 65мс можно сделать uint16_t
uint32_t us[LOG_SIZE];

// =========
uint32_t prevUs;
int count = 0;
bool first, prevS, flag = 0;

void setup() {
  Serial.begin(9600);
  delay(100);
  // запомнили начальное состояние
  first = prevS = digitalRead(IN_PIN);
}

void loop() {
  // читаем время и пин
  uint32_t now = micros();
  bool s = digitalRead(IN_PIN);

  if (prevS != s) {   // состояние пина изменилось
    prevS = s;        // запомнили
    if (!flag) {      // триггер (первое срабатывание)
      flag = 1;       // запомнили
      prevUs = now;   // время первого
    } else {                      // измеряем следующие
      us[count] = now - prevUs;   // время импульса
      prevUs = now;               // запомнили время
      count++;
      // буфер переполнен - вывод лога
      if (count >= LOG_SIZE) logOut();
    }
  }
  // таймаут 1 секунда (сигнал пропал) - вывод лога
  if (flag && now - prevUs > 1000000) logOut();
}

void logOut() {
  // выводим стартовое состояние пина
  Serial.print("fval: ");
  Serial.println(first);

  // и периоды
  for (int i = 0; i < count; i++) {
    Serial.print(us[i]);
    Serial.println(',');
  }
  for (;;);   // висим тут
}