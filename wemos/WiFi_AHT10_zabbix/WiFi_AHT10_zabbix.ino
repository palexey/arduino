#include <Adafruit_AHTX0.h>
#include <ESP8266WebServer.h>
#include <Wire.h>
#include <secret_wifi.h>
#include <ESP8266ZabbixSender.h>

Adafruit_AHTX0 aht;

ESP8266WebServer server(80);
ESP8266ZabbixSender zSender;

uint8_t water_S = 1;
uint8_t zumer_S = D7;

void setup() {
  Serial.begin(115200);
  delay(100);

  Wire.begin(D5, D6);

  /*
TwoWire * wire = &Wire
int32_t sensor_id = 0
uint8_t i2c_address = 56
*/

  if (aht.begin()) {
    Serial.println("Found AHT10");
  } else {
    Serial.println("Not found AHT10");
  }

  pinMode(water_S, INPUT);
  pinMode(zumer_S, OUTPUT);

  Serial.println("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  // проверить, выполнено ли подключение wi-fi сети
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");
  Serial.println(WiFi.localIP());

  //server.on("/", handle_OnConnect);
  //server.onNotFound(handle_NotFound);

  //server.begin();
  //Serial.println("HTTP server started");

  zSender.Init(IPAddress(89, 223, 66, 209), 10051, "Server room1");
}
void loop() {

  //  server.handleClient();
  int water_status = analogRead(water_S);  // считать напряжение с пина A0
  Serial.print("Water Sensor status");
  Serial.println(water_status);

  sensors_event_t humidity, temp;
  aht.getEvent(&humidity, &temp);

  float temp_S = temp.temperature;
  float humi_S = humidity.relative_humidity;

  zSender.ClearItem();                                           // Clear ZabbixSender's item list
  zSender.AddItem("humi_S", (float)humidity.relative_humidity);  // Exmaple value of zabbix trapper item
  zSender.AddItem("temp_S", (float)temp.temperature);            // Exmaple value of zabbix trapper item

  if (zSender.Send() == EXIT_SUCCESS) {  // Send zabbix items
    Serial.println("ZABBIX SEND: OK");
  } else {
    Serial.println("ZABBIX SEND: NG");
  }


  delay(5000);
}

/*
void handle_OnConnect() {
}

void handle_NotFound() {
  server.send(404, "text/plain", "Not found");
}

String SendHTML(float temp_S, float humi_S) {
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr += "<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr += "<title>ESP8266 Weather Station</title>\n";
  ptr += "<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr += "body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;}\n";
  ptr += "p {font-size: 24px;color: #444444;margin-bottom: 10px;}\n";
  ptr += "</style>\n";
  ptr += "</head>\n";
  ptr += "<body>\n";
  ptr += "<div id=\"webpage\">\n";
  ptr += "<h1>ESP8266 Weather Station</h1>\n";
  ptr += "<p>Temperature: ";
  ptr += temp_S;
  ptr += "&deg;C</p>";
  ptr += "<p>Humidity: ";
  ptr += humi_S;
  ptr += "%</p>";

  ptr +="<p>Pressure: ";
  ptr +=pressure;
  ptr +="hPa</p>";
  ptr +="<p>Altitude: ";
  ptr +=altitude;
  ptr +="m</p>";

  ptr += "</div>\n";
  ptr += "</body>\n";
  ptr += "</html>\n";
  return ptr;
}
*/