#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

#include <secret_wifi.h>

const int output = D3;
String sliderValue = "0";
String sliderValue_d2 = "0";
const char* PARAM_INPUT = "value";
const char* PARAM_INPUT_d2 = "value_d2";
AsyncWebServer server(80);

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ESP Web Server</title>
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 2.3rem;}
    p {font-size: 1.9rem;}
    body {max-width: 400px; margin:0px auto; padding-bottom: 25px;}
    .slider { -webkit-appearance: none; margin: 14px; width: 360px; height: 25px; background: #FFD65C;
      outline: none; -webkit-transition: .2s; transition: opacity .2s;}
    .slider::-webkit-slider-thumb {-webkit-appearance: none; appearance: none; width: 35px; height: 35px; background: #003249; cursor: pointer;}
    .slider::-moz-range-thumb { width: 35px; height: 35px; background: #003249; cursor: pointer; } 
  </style>
</head>
<body>
  <h2>ESP Web Server</h2>
  <p><span id="textSliderValue">%SLIDERVALUE%</span></p>
  <p><input type="range" onchange="updateSliderPWM(this)" id="pwmSlider" min="0" max="1023" value="%SLIDERVALUE%" step="1" class="slider"></p>
  <p><span id="textSliderValue_d2">%SLIDERVALUE_d2%</span></p>
  <p><input type="range" onchange="updateSliderPWM_d2(this)" id="pwmSlider_d2" min="0" max="10" value="%SLIDERVALUE_d2%" step="1" class="slider"></p>
<script>
function updateSliderPWM(element) {
  var sliderValue = document.getElementById("pwmSlider").value;
  document.getElementById("textSliderValue").innerHTML = sliderValue;
  console.log(sliderValue);
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "/slider?value="+sliderValue, true);
  xhr.send();
}
function updateSliderPWM_d2(element) {
  var sliderValue_d2 = document.getElementById("pwmSlider_d2").value;
  document.getElementById("textSliderValue_d2").innerHTML = sliderValue_d2;
  console.log(sliderValue_d2);
  var xhr_d2 = new XMLHttpRequest();
  xhr_d2.open("GET", "/slider?value="+sliderValue_d2, true);
  xhr_d2.send();
}
</script>
</body>
</html>
)rawliteral";

// Replaces placeholder with button section in your web page
String processor(const String& var) {
  //Serial.println(var);
  if (var == "SLIDERVALUE") {
    return sliderValue;
  }
  if (var == "SLIDERVALUE_d2") {
    return sliderValue_d2;
  }
  return String();
}

const int ledPin = D1;             // Pin number for the LED
const int fadeDelay = 10;          // Delay between brightness changes in milliseconds
const int fadeAmount = 1;          // Amount of brightness change in each step
int brightness = 0;                // Current brightness of the LED
int fadeDirection = 1;             // Direction of brightness change (1 for increasing, -1 for decreasing)
unsigned long previousMillis = 0;  // Variable to store the previous time

const int ledPin2 = D2;             // Pin number for the LED
int fadeDelay2;                     //  = 100;         // Delay between brightness2 changes in milliseconds
const int fadeAmount2 = 5;          // Amount of brightness2 change in each step
int brightness2 = 0;                // Current brightness2 of the LED
int fadeDirection2 = 1;             // Direction of brightness2 change (1 for increasing, -1 for decreasing)
unsigned long previousMillis2 = 0;  // Variable to store the previous time

const int ledPin6 = D7;             // Pin number for the LED
const int fadeDelay6 = 1;           // Delay between brightness2 changes in milliseconds
const int fadeAmount6 = 1;          // Amount of brightness2 change in each step
int brightness6 = 254;              // Current brightness2 of the LED
int fadeDirection6 = 1;             // Direction of brightness2 change (1 for increasing, -1 for decreasing)
unsigned long previousMillis6 = 0;  // Variable to store the previous time

void setup() {
  Serial.begin(115200);
  pinMode(ledPin, OUTPUT);   // Set LED pin as an output
  pinMode(ledPin2, OUTPUT);  // Set LED pin as an output
  pinMode(ledPin6, OUTPUT);  // Set LED pin as an output


  //////Web status
  analogWrite(output, sliderValue.toInt());

  int fadeDelay2 = sliderValue_d2.toInt();

  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send_P(200, "text/html", index_html, processor);
  });

  // Send a GET request to <ESP_IP>/slider?value=<inputMessage>
  server.on("/slider", HTTP_GET, [](AsyncWebServerRequest* request) {
    String inputMessage;
    // GET input1 value on <ESP_IP>/slider?value=<inputMessage>
    if (request->hasParam(PARAM_INPUT)) {
      inputMessage = request->getParam(PARAM_INPUT)->value();
      sliderValue = inputMessage;
      analogWrite(output, sliderValue.toInt());
    } else {
      inputMessage = "No message sent";
    }

    Serial.println(inputMessage);
    request->send(200, "text/plain", "OK");
  });

  // Start server
  server.begin();
}
void loop() {
  unsigned long currentMillis = millis();  // Get the current time
  // Check if it's time to change the brightness
  if (currentMillis - previousMillis >= fadeDelay) {
    previousMillis = currentMillis;            // Save the current time
    brightness += fadeAmount * fadeDirection;  // Update brightness
    // Reverse the fade direction if brightness reaches its limits
    if (brightness <= 0 || brightness >= 255) {
      fadeDirection = -fadeDirection;
    }
  }
  analogWrite(ledPin, brightness);  // Set LED brightness

  if (currentMillis - previousMillis2 >= fadeDelay2) {
    previousMillis2 = currentMillis;              // Save the current time
    brightness2 += fadeAmount2 * fadeDirection2;  // Update brightness2
    // Reverse the fade direction if brightness2 reaches its limits
    if (brightness2 <= 0 || brightness2 >= 255) {
      fadeDirection2 = -fadeDirection2;
    }
  }
  analogWrite(ledPin2, brightness2);  // Set LED brightness

  if (currentMillis - previousMillis6 >= fadeDelay6) {
    previousMillis6 = currentMillis;              // Save the current time
    brightness6 += fadeAmount6 * fadeDirection6;  // Update brightness2
    // Reverse the fade direction if brightness2 reaches its limits
    if (brightness6 <= 0 || brightness6 >= 255) {
      fadeDirection6 = -fadeDirection6;
    }
  }
  analogWrite(ledPin6, brightness6);  // Set LED brightness
}