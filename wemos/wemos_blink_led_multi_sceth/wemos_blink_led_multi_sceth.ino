const int ledPin = D1;             // Pin number for the LED
const int fadeDelay = 10;          // Delay between brightness changes in milliseconds
const int fadeAmount = 1;          // Amount of brightness change in each step
int brightness = 0;                // Current brightness of the LED
int fadeDirection = 1;             // Direction of brightness change (1 for increasing, -1 for decreasing)
unsigned long previousMillis = 0;  // Variable to store the previous time

const int ledPin2 = D2;             // Pin number for the LED
const int fadeDelay2 = 100;         // Delay between brightness2 changes in milliseconds
const int fadeAmount2 = 5;          // Amount of brightness2 change in each step
int brightness2 = 0;                // Current brightness2 of the LED
int fadeDirection2 = 1;             // Direction of brightness2 change (1 for increasing, -1 for decreasing)
unsigned long previousMillis2 = 0;  // Variable to store the previous time

const int ledPin6 = D7;             // Pin number for the LED
const int fadeDelay6 = 1;          // Delay between brightness2 changes in milliseconds
const int fadeAmount6 = 1;          // Amount of brightness2 change in each step
int brightness6 = 254;                // Current brightness2 of the LED
int fadeDirection6 = 1;             // Direction of brightness2 change (1 for increasing, -1 for decreasing)
unsigned long previousMillis6 = 0;  // Variable to store the previous time

void setup() {
  Serial.begin(115200);
  pinMode(ledPin, OUTPUT);   // Set LED pin as an output
  pinMode(ledPin2, OUTPUT);  // Set LED pin as an output
  pinMode(ledPin6, OUTPUT);  // Set LED pin as an output
}
void loop() {
  unsigned long currentMillis = millis();  // Get the current time
  // Check if it's time to change the brightness
  if (currentMillis - previousMillis >= fadeDelay) {
    previousMillis = currentMillis;            // Save the current time
    brightness += fadeAmount * fadeDirection;  // Update brightness
    // Reverse the fade direction if brightness reaches its limits
    if (brightness <= 0 || brightness >= 255) {
      fadeDirection = -fadeDirection;
    }
  }
  analogWrite(ledPin, brightness);  // Set LED brightness
  Serial.println(brightness);

  if (currentMillis - previousMillis2 >= fadeDelay2) {
    previousMillis2 = currentMillis;              // Save the current time
    brightness2 += fadeAmount2 * fadeDirection2;  // Update brightness2
    // Reverse the fade direction if brightness2 reaches its limits
    if (brightness2 <= 0 || brightness2 >= 255) {
      fadeDirection2 = -fadeDirection2;
    }
  }
  analogWrite(ledPin2, brightness2);  // Set LED brightness
  Serial.print(',');
  Serial.print(brightness2);

  if (currentMillis - previousMillis6 >= fadeDelay6) {
    previousMillis6 = currentMillis;              // Save the current time
    brightness6 += fadeAmount6 * fadeDirection6;  // Update brightness2
    // Reverse the fade direction if brightness2 reaches its limits
    if (brightness6 <= 0 || brightness6 >= 255) {
      fadeDirection6 = -fadeDirection6;
    }
  }
  analogWrite(ledPin6, brightness6);  // Set LED brightness
  Serial.print(',');
  Serial.print(brightness6);
}