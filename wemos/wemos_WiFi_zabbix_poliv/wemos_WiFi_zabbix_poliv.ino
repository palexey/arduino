#include <ESP8266WebServer.h>
#include <secret_wifi.h>
#include <ESP8266ZabbixSender.h>
#include <CD74HC4067.h>

ESP8266WebServer server(80);
ESP8266ZabbixSender zSender;


CD74HC4067 mux(D5, D6, D7, D8);
//const int a_pin = A0

const int AirValue0 = 739;
const int AirValue1 = 750;
const int AirValue2 = 740;

const int WaterValue0 = 316;
const int WaterValue1 = 310;
const int WaterValue2 = 316;

int soilMoistureValue0 = 0;
int soilMoistureValue0_prev = 0;

int soilMoistureValue1 = 0;
int soilMoistureValue1_prev = 0;

int soilMoistureValue2 = 0;
int soilMoistureValue2_prev = 0;

int soilmoisturepercent0 = 0;
int soilmoisturepercent1 = 0;
int soilmoisturepercent2 = 0;

void setup() {
  Serial.begin(115200);
  delay(100);

//  Wire.begin(D5, D6);

  /*
TwoWire * wire = &Wire
int32_t sensor_id = 0
uint8_t i2c_address = 56


  if (aht.begin()) {
    Serial.println("Found AHT10");
  } else {
    Serial.println("Not found AHT10");
  }
*/
  
 
//  pinMode(water_S4, INPUT);
//  pinMode(water_S3, INPUT);
//  pinMode(water_S2, INPUT);
//  pinMode(zumer_S, OUTPUT);

  Serial.println("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  // проверить, выполнено ли подключение wi-fi сети
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");
  Serial.println(WiFi.localIP());

  server.on("/", handle_OnConnect);
  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server started");

  //zSender.Init(IPAddress(89, 223, 66, 209), 10051, "Server room1");
  zSender.Init(IPAddress(89, 223, 66, 209), 10051, "home Fryazino");
}
void loop() {
  server.handleClient();

  mux.channel(0);
  soilMoistureValue0 = analogRead(A0);
  delay(100);
  mux.channel(1);
  soilMoistureValue1 = analogRead(A0);
  delay(100);
  mux.channel(2);
  soilMoistureValue2 = analogRead(A0);

  if (soilMoistureValue0_prev != soilMoistureValue0 || soilMoistureValue1_prev != soilMoistureValue1 || soilMoistureValue2_prev != soilMoistureValue2 ){

    zSender.ClearItem();                                           // Clear ZabbixSender's item list

    if (soilMoistureValue1_prev != soilMoistureValue1 ){
      Serial.println(soilMoistureValue0);
      soilmoisturepercent0 = map(soilMoistureValue0, AirValue0, WaterValue0, 0, 100);
      Serial.print("Soil0 Moisture0 current value in %:  ");
      Serial.println(soilmoisturepercent0);
      zSender.AddItem("water_status1", (float)soilMoistureValue0);  
      soilMoistureValue0_prev = soilMoistureValue0;
    }
    if (soilMoistureValue1_prev != soilMoistureValue1 ){
      Serial.println(soilMoistureValue1);
      soilmoisturepercent1 = map(soilMoistureValue1, AirValue1, WaterValue1, 0, 100);
      Serial.print("Soil1 Moisture1 current value in %:  ");
      Serial.println(soilmoisturepercent1);
      zSender.AddItem("water_status2", (float)soilMoistureValue1);                  
      soilMoistureValue1_prev = soilMoistureValue1;
    }
    if (soilMoistureValue2_prev != soilMoistureValue2){
      Serial.println(soilMoistureValue2);
      soilmoisturepercent2 = map(soilMoistureValue2, AirValue2, WaterValue2, 0, 100);
      Serial.print("Soil2 Moisture2 current value in %:  ");
      Serial.println(soilmoisturepercent2);
      zSender.AddItem("water_status3", (float)soilMoistureValue2);                  
      soilMoistureValue2_prev = soilMoistureValue2;
    }

    if (zSender.Send() == EXIT_SUCCESS) {  // Send zabbix items
      Serial.println("ZABBIX SEND: OK");
    } 
    else {
      Serial.println("ZABBIX SEND: NG");
    }
  }
  delay(5000);  


/*
  mux.channel(1);
  soilMoistureValue1 = analogRead(A0);
    
  delay(2000);

  mux.channel(2);
  soilMoistureValue2 = analogRead(A0);
   
  delay(2000);



  for (int i = 0; i < 16; i++) {
    mux.channel(i); // Устанавливаем мультиплексор на канал i
 
    // Считываем аналоговое значение с пина A0
    int value = analogRead(A0);
 
    // Выводим информацию о текущем канале и считанном значении
    Serial.print("Канал ");
    Serial.print(i);
    Serial.print(": ");
    Serial.println(value);
    
    delay(1000); // Пауза 1 секунда между считыванием значений с разных каналов
  }  
  
  int water_status4 = digitalRead(water_S4);
  int water_status3 = digitalRead(water_S3);
  int water_status2 = digitalRead(water_S2);

  Serial.print("Water Sensor 4 status");
  Serial.println(water_status4);

  Serial.print("Water Sensor 3 status");
  Serial.println(water_status3);


  delay(5000);
*/  
}


void handle_OnConnect() {
}

void handle_NotFound() {
  server.send(404, "text/plain", "Not found");
}

String SendHTML(float temp_S, float humi_S) {
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr += "<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr += "<title>ESP8266 Weather Station</title>\n";
  ptr += "<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr += "body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;}\n";
  ptr += "p {font-size: 24px;color: #444444;margin-bottom: 10px;}\n";
  ptr += "</style>\n";
  ptr += "</head>\n";
  ptr += "<body>\n";
  ptr += "<div id=\"webpage\">\n";
  ptr += "<h1>ESP8266 Weather Station</h1>\n";
  ptr += "<p>Горшок один: ";
  ptr += soilmoisturepercent0;
  ptr += "&deg;C</p>";
  ptr += "<p>Горшок два: ";
  ptr += soilmoisturepercent1;
  ptr += "%</p>";
  ptr +="<p>Горшок три: ";
  ptr += soilmoisturepercent2;
  ptr +="hPa</p>";
//  ptr +="<p>Altitude: ";
// ptr +=altitude;
//  ptr +="m</p>";

  ptr += "</div>\n";
  ptr += "</body>\n";
  ptr += "</html>\n";
  return ptr;
}
