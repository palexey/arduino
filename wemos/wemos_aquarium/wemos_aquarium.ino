#include <microDS18B20.h>
#include <ESP8266WebServer.h>
#include <ESP8266ZabbixSender.h>
#include <secret_wifi.h>

#include <Wire.h>
#include <DHT.h>
#include <DHT_U.h>


MicroDS18B20<D4> sensor;

DHT dht(D3, DHT11);  // сообщаем на каком порту будет датчик
ESP8266WebServer server(80);
ESP8266ZabbixSender zSender;

float z_water_temp;
float z_water_temp_prev;
float z_air_temp;
float z_air_temp_prev;

void setup() {
  Serial.begin(115200);
  dht.begin();                // запускаем датчик DHT11

  //WiFi
  Serial.println("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");
  Serial.println(WiFi.localIP());

  // Web server
  /*
    server.on("/", handle_OnConnect);
    server.onNotFound(handle_NotFound);
    server.begin();
    Serial.println("HTTP server started");
  */

  zSender.Init(IPAddress(217, 25, 91, 146), 10051, "Aquarium");
}
void loop() {
  //старт веб-сервера
  //  server.handleClient();
  // Clear ZabbixSender's item list
  zSender.ClearItem();
  // запрос температуры
  sensor.requestTemp();
  z_air_temp = dht.readTemperature();
  float z_humi_val = dht.readHumidity();
  delay(1000);
  //проверяем успешность чтения и выводим
  if (sensor.readTemp()) {
    Serial.println(sensor.getTemp());
    z_water_temp = sensor.getTemp();
    if (z_water_temp_prev != z_water_temp) {
      Serial.println(z_water_temp);
      zSender.AddItem("water_temp", (float)z_water_temp);
      z_water_temp_prev = z_water_temp;
      if (zSender.Send() == EXIT_SUCCESS) {  // Send zabbix items
        Serial.println("ZABBIX SEND: OK");
      } else Serial.println("ZABBIX SEND: NG");
    }
  } else Serial.println("error");
}
