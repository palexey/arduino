#include <Wire.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include "zabbixSender.h"
#include "zabbixSettings.h"

#include <AHT10.h>

AHT10class sd;


// ****** Functions *******
void goDeepsleep() {
  WiFi.disconnect(true);
  delay(100);
  esp_sleep_enable_timer_wakeup(sendDataIntervallSec * 1000 * 1000ULL);
  Serial.printf("Deepsleep starts now for %d seconds.\n", sendDataIntervallSec);
  delay(100);
  esp_deep_sleep_start();
}

// ****** Main *******

void setup() {
  Serial.begin(115200);
  delay(100);
  Serial.println("=== SETUP Start ===");

  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);

  Serial.println("Waiting for WiFi... ");
  // indicate to user
  digitalWrite(GREEN_LED, HIGH);
  WiFi.begin(ssid, wifiKey);
  // current wifi ap connect cycles
  int wccycles = 0;
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    Serial.print((int)WiFi.status());
    wccycles++;
    // try maxWifiConnectcycles than deepsleep
    if (wccycles > maxWifiConnectcycles) {
      Serial.printf("\n[ERROR] WLAN TIMEOUT - Abort\n");
      // indicate to user
      digitalWrite(GREEN_LED, LOW);
      digitalWrite(RED_LED, HIGH);
      delay(1000);
      digitalWrite(RED_LED, LOW);
      goDeepsleep();
    }
    delay(300);
  }

  // we are connected, turn off
  digitalWrite(GREEN_LED, LOW);
  Serial.printf("\nWiFi connected. IP address:");
  Serial.println(WiFi.localIP());
  Serial.println("=== SETUP End ===");
}  // setup



void loop() {
  Serial.println("=== START ===");
  // init sensor

  bool sbStatus = sb.begin();
  if (!sbStatus) {
    Serial.printf("[ERROR] Sensor FAILURE - Abort\n");
    // indicate to user
    digitalWrite(RED_LED, HIGH);
    delay(1000);
    digitalWrite(RED_LED, LOW);
    goDeepsleep();
  }

//  BMESensorData_t sd = sb.getData();
  Serial.printf("Collected values. temp:%f, hum:%f, press:%f\n", sd.temp, sd.hum);

  ZabbixSender zs;
  String jsonPayload;
  jsonPayload = zs.createPayload(hostname, sd.temp, sd.hum);
  // create message in zabbix sender format
  String msg = zs.createMessage(jsonPayload);

  // connect and transmit
  Serial.printf("==> Connecting to:%s:%d\n", server, port);
  // connect to server
  WiFiClient client;
  // indicate to user
  digitalWrite(GREEN_LED, HIGH);
  if (!client.connect(server, port)) {
    Serial.printf("[ERROR] Connection FAILED - Abort\n");
    digitalWrite(GREEN_LED, LOW);
    digitalWrite(RED_LED, HIGH);
    delay(1000);
    digitalWrite(RED_LED, LOW);
    goDeepsleep();
  }

  Serial.println("Connected. Sending data.");
  client.print(msg);
  unsigned long startTime = millis();
  while (client.available() == 0) {
    if (millis() - startTime > maxTransmissionTimeout) {
      Serial.printf("[ERROR] Transmission TIMEOUT - Abort\n");
      client.stop();
      // indicate to user
      digitalWrite(GREEN_LED, LOW);
      digitalWrite(RED_LED, HIGH);
      delay(1000);
      digitalWrite(RED_LED, LOW);
      goDeepsleep();
    }
  }

  digitalWrite(GREEN_LED, LOW);

  // read reply from zabbix server
  while (client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  Serial.println();

  Serial.printf("Closing connection - Sleeping for %d sec...\n", sendDataIntervallSec);
  client.stop();
  goDeepsleep();
}
