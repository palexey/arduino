#include <ESP8266WiFi.h>
#include <secret_wifi.h>

//WiFiServer server(80);
 
void setup() {
    // иницилизируем монитор порта
    Serial.begin(9600);

    WiFi.mode(WIFI_STA);
    WiFi.disconnect();

    // подключаемся к Wi-Fi сети
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.println(ssid);
        Serial.println(password);
        Serial.println("Connecting to Wi-Fi..");
    }
    Serial.println("Connected to the Wi-Fi network");
    Serial.println(WiFi.localIP());
  
}
 
void loop() {
    
}
