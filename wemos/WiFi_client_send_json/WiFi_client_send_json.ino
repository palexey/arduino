/*
Для отправки данных датчика AHT10 на сервер Zabbix с использованием ESP8266, вам потребуется выполнить следующие шаги:
1. Подключите ESP8266 к вашей сети Wi-Fi и установите необходимые библиотеки для работы с датчиком AHT10 и протоколом HTTP.
2. Инициализируйте датчик AHT10 и получите значения его данных (температура и влажность).
3. Создайте HTTP POST-запрос с данными датчика AHT10 и отправьте его на сервер Zabbix. Для этого вам потребуется знать IP-адрес и порт сервера Zabbix, а также путь к API Zabbix.
4. Обработайте ответ сервера Zabbix, чтобы убедиться, что данные были успешно отправлены.
Ниже приведен пример кода на языке Arduino, который демонстрирует, как выполнить эти шаги:
```cpp
*/
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <AHT10.h>
#include <secret_wifi.h>

const char* zabbixServer = "zb.euroexpo.ru"; // IP-адрес сервера Zabbix
const int zabbixPort = 443; // Порт сервера Zabbix
const char* zabbixPath = "/api/zabbix"; // Путь к API Zabbix

AHT10class aht;

void setup() {
  Serial.begin(115200);
  // Подключение к Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  // Инициализация датчика AHT10
  Wire.begin();
  
  if(AHT10.begin(0x38))                            // Инициализируем датчик с адресом 0x38
    Serial.println("AHT10 подключен.");            // Если удачно печатаем "AHT10 подключен."
  else{
    Serial.println("AHT10 не подключен.");         // Если не удачно печатаем "AHT10 не подключен."
    while(1);
}
void loop() {
  // Получение значений температуры и влажности от датчика AHT10
  float temp = aht.getTemperature();
  float hum = aht.getHumidity();
  // Создание JSON-строки с данными датчика AHT10
  String jsonData = "{\"temperature\":" + String(temp) + ",\"humidity\":" + String(hum) + "}";
  // Создание HTTP POST-запроса
  String url = "https://" + String(zabbixServer) + ":" + String(zabbixPort) + String(zabbixPath);
  String payload = "data=" + jsonData;
  
  // Отправка HTTP POST-запроса на сервер Zabbix
  WiFiClient client;
  if (client.connect(zabbixServer, zabbixPort)) {
    client.println("POST " + String(zabbixPath) + " HTTP/1.1");
    client.println("Host: " + String(zabbixServer));
    client.println("Content-Type: application/x-www-form-urlencoded");
    client.println("Connection: close");
    client.println("Content-Length: " + String(payload.length()));
    client.println();
    client.println(payload);
    client.println();
  }
  // Ожидание ответа от сервера Zabbix
  while (client.connected() && !client.available()) {
    delay(100);
  }
  // Чтение и обработка ответа сервера Zabbix
  while (client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  // Закрытие соединения с сервером Zabbix
  client.stop();
  // Ожидание 5 секунд перед повторной отправкой данных
  delay(5000);
}
