#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
// Wi-Fi параметры
const char* ssid = "your_SSID";
const char* password = "your_PASSWORD";
// JSON буфер
StaticJsonBuffer<200> jsonBuffer;
// Создание сервера
ESP8266WebServer server(80);
void handleRoot() {
  // Создание JSON объекта
  JsonObject& root = jsonBuffer.createObject();
  
  // Добавление данных в JSON объект
  root["sensor"] = "temperature";
  root["value"] = 25.5;
  
  // Преобразование JSON в строку
  String jsonString;
  root.printTo(jsonString);
  
  // Отправка JSON на сервер
  WiFiClient client;
  if (client.connect("your_server_address", 80)) {
    client.println("POST /data HTTP/1.1");
    client.println("Host: your_server_address");
    client.println("Content-Type: application/json");
    client.print("Content-Length: ");
    client.println(jsonString.length());
    client.println();
    client.println(jsonString);
  }
  
  // Закрытие соединения
  client.stop();
  
  // Отправка ответа клиенту
  server.send(200, "text/plain", "Data sent to server");
}
void setup() {
  // Подключение к Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  
  // Настройка маршрутов сервера
  server.on("/", handleRoot);
  
  // Запуск сервера
  server.begin();
  Serial.println("Server started");
}
void loop() {
  // Обработка запросов клиентов
  server.handleClient();
}
```
В данном коде используется библиотека `ArduinoJson` для работы с JSON. В функции `handleRoot()` создается JSON объект, добавляются данные в него, преобразуется в строку и отправляется на сервер с помощью `WiFiClient`.
Необходимо заменить переменные `your_SSID`, `your_PASSWORD` и `your_server_address` на соответствующие значения для вашей сети Wi-Fi и сервера.
При обращении к корневому URL сервера, будет вызываться функция `handleRoot()`, которая формирует и отправляет JSON на сервер. В ответ клиенту отправляется сообщение "Data sent to server".
После загрузки кода на ESP8266, вы сможете видеть логи в мониторе последовательного порта Arduino IDE, чтобы узнать, происходит ли подключение к Wi-Fi и отправка данных на сервер.