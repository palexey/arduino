 // создаем переменные, хранящие номера наших пинов
#define RED 11
#define GRN 12
#define BLU 6
 
void setup() {

     // обозначаем что наши пины работают как выходы
    pinMode(RED, OUTPUT);
    pinMode(GRN, OUTPUT);
    pinMode(BLU, OUTPUT);
}
 
void loop() {
   digitalWrite(RED, 1); // включаем красный свет
   digitalWrite(GRN, 0);
   digitalWrite(BLU, 0);
 
   delay(1000); // устанавливаем паузу для эффекта
 
   digitalWrite(RED, 0);
   digitalWrite(GRN, 1); // включаем зеленый свет
   digitalWrite(BLU, 0);
 
   delay(1000); // устанавливаем паузу для эффекта
 
   digitalWrite(RED, 0);
   digitalWrite(GRN, 0);
   digitalWrite(BLU, 1); // включаем синий свет
 
}



// Объявляем нашу функцию для управления светодиодом
//void RGB_color(int red_value, int green_value, int blue_value)
//  {
//    analogWrite(red_pin, red_value);
//    analogWrite(green_pin, green_value);
//    analogWrite(blue_pin, blue_value);
//}